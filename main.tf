resource "azurerm_resource_group" "resource-group-490b8e9b" {
  location = "East US"

  tags = {
    env      = "test"
    archUUID = "77182dae-4535-418f-98c9-a034315c98d6"
  }
}

resource "azurerm_app_service" "azurerm_app_service-4df19f14" {
  resource_group_name = azurerm_resource_group.resource-group-490b8e9b.name
  location            = "East US"

  tags = {
    env      = "test"
    archUUID = "77182dae-4535-418f-98c9-a034315c98d6"
  }
}

